# Ansible Constenv Playbook

Build your development environment.

## Description

This Ansible Playbook uses following tools to build a development environment.

- git
- vim
- tmux
- zsh
- ansible
- ruby
- python

## Requirement

- Ansible

## Usage

1. Clone playbook
2. Run playbook

## Installation
### clone repo
```sh
$ git clone https://github.com/Tocyuki/ansible_constenv_playbook
```

### run playbook
- ex.) for local

```sh
$ ansible-playbook construction.yml --conection=local --extra-vars 'hostsname=localhost' --skip-tags 'ansible'
```

- ex.) for remote

```sh
$ ansible-playbook -i hosts/$HOSTS_FILE construction.yml --extra-vars 'hostsname=$HOSTSNAME'
```

- ex.) when executing only specific tasks

```sh
$ ansible-playbook -i hosts/$HOSTS_FILE construction.yml --extra-vars 'hostsname=$HOSTSNAME' --tags 'git,vim,zsh'
```

## Anything Else
When installing ruby and python with playbook, you need to install zsh in advance as a playbook.

## Author

[@Tocyuki](https://twitter.com/Tocyuki)

## License

[MIT](https://tocyuki.mit-license.org)
